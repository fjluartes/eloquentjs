// script data set
let SCRIPTS = require("../../../scripts.js");

function characterCount(script) {
  return script.ranges.reduce((count, [from, to]) => {
    return count + (to - from);
  }, 0);
}
// console.log(SCRIPTS.reduce((a, b) => {
//   return characterCount(a) < characterCount(b) ? b : a;
// }));

// Composability
function average(array) {
  return array.reduce((a, b) => a + b) / array.length;
}
// console.log(Math.round(average(
//   SCRIPTS.filter(s => s.living).map(s => s.year))));
// console.log(Math.round(average(
//   SCRIPTS.filter(s => !s.living).map(s => s.year))));

function characterScript(code) {
  for (let script of SCRIPTS) {
    if (script.ranges.some(([from, to]) => {
      return code >= from && code < to;
    })) {
      return script;
    }
  }
  return null;
}
// console.log(characterScript(121));
