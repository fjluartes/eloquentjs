// Chessboard: 8x8 grid with # and spaces. Size = 8 but can be changed to any integer.
// let size = prompt("Board size");
let size = 8;
function chessboard(size) {
  for (let i = 0; i < size; i++){
    let line = "";
    for (let j = 0; j < size; j++) {
      if (!isEven(i)) {
        if (isEven(j)) {
          line += '#';
        } else {
          line += ' ';
        }
      } else {
        if (isEven(j)) {
          line += ' ';
        } else {
          line += '#';
        }
      }
    }
    console.log(line);
  }
}

function isEven(n) {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}

chessboard(size);
