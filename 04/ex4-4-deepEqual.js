// deepEqual: Compare properties of objects
function deepEqual(obj1, obj2) {
  if (isObject(obj1) && isObject(obj2)) {
    let keys = Object.keys(obj1);
    for (let k of keys) {
      if (isObject(obj1[k]) && isObject(obj2[k])) {
        deepEqual(obj1[k], obj2[k]);
      } else {
        return obj1[k] === obj2[k];
      }
    }
    return false;
  } else {
    return false;
  }
}

function isObject(obj) {
  if (typeof(obj) == "object" && obj != null) {
    return true;
  } else {
    return false;
  }
}

let obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// true
console.log(deepEqual(obj, {here: 1, object: 2}));
// false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// true
