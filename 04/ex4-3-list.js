// List: Create arrayToList function that converts an array to a list
// Create listToArray function that converts a list to an array
// Create prepend function to add an element to a list
// Create nth function to return the position of an element in a list
function arrayToList(arr) {
  let list = {}, temp = {};
  let l = arr.length - 1;
  for (let i = l; i >= 0; i--) {
    if (i == l) {
      temp = {
        value: arr[i],
        rest: null
      };
    } else {
      temp = {
        value: arr[i],
        rest: list
      };
    }
    list = temp;
  }
  return list;
}

function listToArray(list) {
  let arr = [];
  let val = list.value;
  arr.push(val);
  let temp = list.rest;
  while (temp !== null) {
    val = temp.value;
    arr.push(val);
    temp = temp.rest;
  }
  return arr;
}

function prepend(value, rest) {
  let list = {
    value,
    rest
  };
  return list;
}

function nth(list, index) {
  let arr = listToArray(list);
  let res = arr[index];
  if (res == undefined) {
    return undefined;
  } else {
    return res;
  }
}

// recursive version of nth
function nth2(list, index) {
  function nthIter(list, index, ctr) {
    if (list.rest == null) {
      if (ctr == index) {
        return list.value;
      }
      return undefined;
    } else {
      if (ctr == index) {
        return list.value;
      }
      ctr++;
      return nthIter(list.rest, index, ctr);
    }
  }
  return nthIter(list, index, 0);
}

console.log(arrayToList([10, 20]));
// {value: 10, rest: {value: 20, rest: null}}
console.log(listToArray(arrayToList([10, 20, 30])));
// [10, 20, 30]
console.log(prepend(10, prepend(20, null)));
// {value: 10, rest: {value: 20, rest: null}}
console.log(nth(arrayToList([10, 20, 30]), 1));
// 20
